<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SetRatingValue</fullName>
        <description>Sets the rating value as selected by the rating picklist</description>
        <field>Rating_Value__c</field>
        <formula>CASE( Rating__c , 
&quot;Love It&quot;, 5,
&quot;Really Like It&quot;, 4,
&quot;Like It&quot;, 3,
&quot;Don&apos;t Like It&quot;, 2,
&quot;Hate It&quot;, 1,
0)</formula>
        <name>SetRatingValue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SetRatingValue</fullName>
        <actions>
            <name>SetRatingValue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Exhibition_Member__c.Rating__c</field>
            <operation>equals</operation>
            <value>Don&apos;t Like It,Really Like It,Hate It,Love It,Like It</value>
        </criteriaItems>
        <description>Sets the value for the selected rating</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
