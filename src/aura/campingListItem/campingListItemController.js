({
	packItem : function(component, event, helper) {
		var comp = component.get("v.Item");
        comp.Name = "Teste";
        comp.Packed__c = true;
        comp.Quantity__c = 19;
        comp.Price__c = 20;
        component.set("v.Item",comp);
        event.getSource().set("v.disabled",true);
	}
})