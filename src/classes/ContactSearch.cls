public class ContactSearch {
    public static List<Contact> searchForContacts(String name, String postalCode){
        return [Select Id, Name From Contact where LastName =: name AND MailingPostalCode =: postalCode];
    }
}