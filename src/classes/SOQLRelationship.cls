public class SOQLRelationship {
	
    public static void soql(){
        
        Account a = new Account(Name='Teste');
        insert a;
        
        Contact c = new Contact(LastName = 'LastTest');
        c.AccountId = a.Id;
        insert c;
        
        c = [Select Account.Name from Contact where Id =: c.Id ];
         c.Account.Name = 'Sales Force';
        c.LastName = 'Roth';
        
        update c;
        update c.Account;
    }
    
    public static void soql2(){
   
		AggregateResult[] groupResult = [Select avg(Amount)aver from Opportunity];
        Object avgAmount = groupResult[0].get('aver');
        
        System.debug(avgAmount);
           
    }
}