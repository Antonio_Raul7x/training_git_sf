@isTest
public class MyHelloWorldTestClass {
    static testMethod void validateHelloWorld(){
        Book__c b = new Book__c(Name='Game Of Thrones', Price__c=100);
        System.debug('Price before insert the new book: ' + b.Price__c);
        
        insert b;
        
        b = [Select Price__c from Book__c Where Id =: b.Id];
        System.debug('Price after Trigger fired: ' + b.Price__c);
        
        System.assertEquals(90, b.Price__c);
    }
}