/******************************************************************************************
                                    SulAmérica - 2015

Classe responsável por criar um webservice para manifestações 

Projeto: CRM - Estruturação Tecnológica
Nome:WebServiceManifestacao
Criado por: Antonio Raul
Data: 18/09/2015

Modificado por: Antonio Raul
Data: 22/09/2015
******************************************************************************************/
global class testeSoap {
    
    
    /*------------------------------------------------------------
    Author:       Antonio Raul 
    Company:       Accenture
    Description:   Metodo responsável por criar um caso e retornar o protocolo desse caso 
    ------------------------------------------------------------*/
    webService static Retorno Executar(String origem, String status, String tipo_de_registro, String area_de_negocio, String idConta, String tipo, String subTipo, String assunto){

   
        
        return null;
    }


    global class Retorno{
        Webservice Integer codigo;
        Webservice String protocolo;
        Webservice String id;
        Webservice Datetime data_de_criacao;
        Webservice String mensagem;
    }
}