@isTest
public class AccountManagerTest {
    static testMethod void myUnitTest(){
       	Id recordId = helpMethod();
        RestRequest request = new RestRequest();
        request.requestURI = 'https://na24.salesforce.com/services/apexrest/Account/'+recordId;
        request.httpMethod = 'GET';
        RestContext.request = request;
        Account thisAccount = AccountManager.getAccount();
        System.assert(thisAccount != null);
        System.assertEquals('Teste', thisAccount.Name);
    }
    
    private static Id helpMethod(){
        Account accountID = new Account(Name='Teste');
        insert accountId;
        return accountId.id;
    }
    
}