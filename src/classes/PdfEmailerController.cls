public with sharing class PdfEmailerController {

    //campos do formulario
    
    public Id selectedAccount{get; set;}//conta selecionada na pagina do viasualforce
    public String selectedReport {get; set;}//relatorio selecionado
    public String recipientEmail {get; set;}//envia para este email
    
    //metodo de acao para o botao [Send Account Summary]
    public PageReference sendReport(){
        /*NOTA: Verificacao de erros para deixar o codigo mais curto, o ideal seria uma verificacao mais robusta*/
        if(String.isBlank(this.selectedAccount) || String.isBlank(this.recipientEmail)){
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                                      'Errors on the form. Please correct and resubmit.'));
                
                return null;
        }
        
        //obtem o nome da conta de email para as mensagens 
        Account account = [Select Name from Account where id =: this.selectedAccount Limit 1];
        
        if(null == account){
            //obteve um id falso do envio do formulario 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                                       'Invalid account. Please correct and resubmit.'));
        	
                return null;
        }
        
        //cria o email
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.setToAddresses(new String[]{this.recipientEmail});
        message.setSubject('Account summary for' + account.Name);
        message.setHtmlBody('Here\'s a summary for the' + account.Name + 'account.');
        
        //cria o PDF
        PageReference reportPage = (PageReference)this.reportPagesIndex.get(this.selectedReport);
        reportPage.getParameters().put('id', this.selectedAccount);
        Blob reportPdf;
        
        try{
            reportPdf = reportPage.getContentAsPDF();
        }catch(Exception e){
            reportPdf = Blob.valueOf(e.getMessage());
        }
        
        //anexar pdf ao email e enviar
   		Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
        attachment.setContentType('application/pdf');
        attachment.setFileName('AccountSummary-' + account.Name + '.pdf');
        attachment.setInline(false);
        attachment.setBody(reportPdf);
        message.setFileAttachments(new Messaging.EmailFileAttachment [] { attachment});
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{message});
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,
                                                  'Email sent with PDF attachment to ' + this.recipientEmail));
       
   		return null;//se mantem na mesma pagina mesmo em caso de sucesso 
    }
    
    /****Form Helpers****/
    
    //Manda as 10 contas recem-criadas para o menu Account
    public List<SelectOption> recentAccounts{
        get{
            if(null == recentAccounts){
                recentAccounts = new List<SelectOption>();
                List<Account> a = [Select Id, Name, LastModifiedDate from Account 
                                   Order By LastModifiedDate Desc Limit 10];
                    
                for(Account acct : a){
                    recentAccounts.add(new SelectOption(acct.Id,acct.Name));
                }
            }
            return recentAccounts;
        }
        set;
    }
    
    //Lista de relatorios disponiveis para o menu de selecao Summary Format
    public List<SelectOption> reportFormats{
        get{
            if(null == reportFormats){
                reportFormats = new List<SelectOption>();
                for(Map<String, Object>report : reports){
                    reportFormats.add(new SelectOption(
                    (String) report.get('name'),(String) report.get('label')));
                }
            }
            return reportFormats;
        }
        set;
    }
    
    /**Private Helpers**/
    
    //Lista de modelos de relatorios que voce pode disponibilizar
    //estas são apenas as paginas do visualforce que voce pode imprimir em formato PDF
    private Map<String, PageReference> reportPagesIndex;
    private List<Map<String,Object>> reports{
        get{
            if(null == reports){
                reports = new List<Map<String,Object>>();
                //adiciona um relatorio na lista de relatorios
                Map<String,Object> simpleReport = new Map<String, Object>();
                simpleReport.put('name', 'simple');
                simpleReport.put('label', 'Simple');
                simpleReport.put('page', Page.ReportAccountSimple);
                reports.add(simpleReport);
                
                this.reportPagesIndex = new Map<String,PageReference>();
                for(Map<String,Object> report : reports) {
					this.reportPagesIndex.put(
								(String)report.get('name'), (PageReference)report.get('page'));
				}
                
            }
            
             return reports;
        }
        set;
       
    }
    
}