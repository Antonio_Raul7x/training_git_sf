global class AnimalLocator {

    
    public static String getAnimalNameById(Integer id){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://th-apex-http-callout.herokuapp.com/animals/'+id);
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        Animal result = (Animal) JSON.deserialize(response.getBody(),Animal.class);
        return result.animal.name;
    }
    
    public class Animal{
        public Animal animal;
        public Integer id;
        public String name;
        public String eats;
        public String says;
    }
}