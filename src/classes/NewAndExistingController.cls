public class NewAndExistingController {
    
    public Account account {get; private set;}

    public NewAndExistingController (){
        
        ID id = ApexPages.currentPage().getParameters().get('id');
        
        account = (id == null) ? new Account() : [Select Name, Phone, Industry from Account 
                                                  where ID =: id ];
    }
    
    public PageReference save(){
        
        try{
            upsert(account);
        }catch(System.DmlException e){
            ApexPages.addMessages(e);
            return null;
        }
        
        return (new ApexPages.StandardController(account)).view();
    }
}