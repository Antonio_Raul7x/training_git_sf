public class MobileInventoryExtension {

    public MobileInventoryExtension(ApexPages.StandardController c) { }
    public MobileInventoryExtension(ApexPages.StandardSetController c) { }
    
     @RemoteAction 
    public static String updateMerchandiseItem(String productId, Integer newInventory){
        List<Merchandise__c> merchandise = [Select Id, Name, Price__c, Quantity__c from Merchandise__c
                                  Where Id =: productId Limit 1];
            Integer i = 0;
            for(Merchandise__c merchis : merchandise) {
                   i++;
            }
            
        if(i > 0){
            
            merchandise[0].Quantity__c = newInventory;
            
         try {
            update merchandise[0];
            return 'Item Updated';
         }
         catch (Exception e) {
            return e.getMessage();
         } 
        } else {
                
                return 'No item found with that ID';
         }
  
    }
   
}