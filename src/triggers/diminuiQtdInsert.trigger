trigger diminuiQtdInsert on Fatura__c (after insert) {
	
     try{
        
         Fatura__c fat = [Select f.Produto__r.ID from Fatura__c f
                          		 where f.ID =: Trigger.New[0].Id Limit 1];
    
         Produto__c prod = [Select ID, Quantidade_em_Estoque__c from Produto__c
                                 where ID =: fat.Produto__r.ID Limit 1];
         
         if(fat.Produto__r.ID == prod.Id & prod.Quantidade_em_Estoque__c > 
           Trigger.New[0].Quantidade_de_Produtos__c){
               
         	prod.Quantidade_em_Estoque__c -= 
                           Trigger.New[0].Quantidade_de_Produtos__c;   
               
               update prod;
           }else{
               Trigger.New[0].AddError('Não há produtos suficientes no estoque!');
           }
         
     }catch(Exception e ){
         Trigger.New[0].AddError(e.getMessage());
     }
}