trigger deleteProdutosIguais on Produto__c (after insert) {
    
   try{
        Produto__c prod = [Select  Quantidade_em_Estoque__c,Pre_o__c,Marca__c from Produto__c 
                    where Name = :Trigger.New[0].Name and
                    ID != :Trigger.New[0].Id and
                    Marca__c = :Trigger.New[0].Marca__c Limit 1];
       
    
        if(prod!=null & prod.Marca__c == Trigger.New[0].Marca__c){
        
            Produto__c prod2 = [Select Quantidade_em_Estoque__c,Pre_o__c from Produto__c 
                    where Name = :Trigger.New[0].Name and
                    ID = :Trigger.New[0].Id Limit 1];
            
            prod2.Quantidade_em_Estoque__c += prod.Quantidade_em_Estoque__c;
            prod2.Pre_o__c = prod.Pre_o__c;
              
            update prod2;
            delete prod;
             
        }
        
   }catch(Exception e){}  
}