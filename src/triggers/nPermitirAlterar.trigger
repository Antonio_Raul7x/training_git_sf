trigger nPermitirAlterar on Fatura__c (before update) {
	
    Fatura__c fat = [Select Status__c from Fatura__c 
                    where ID =: Trigger.New[0].ID];
    
    if(fat.Status__c.equals('Fechada') & Trigger.new[0].Status__c.equals('Fechada')){
        Trigger.New[0].AddError('A fatura já esta fechada!');
    }
}