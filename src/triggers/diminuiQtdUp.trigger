/*
Trigger para diminuir o numero de produtos em estoque sempre que for adicionado produtos a
fatura
*/
trigger diminuiQtdUp on Fatura__c (before update) {

    try{
        
         Fatura__c fat = [Select f.Produto__r.ID from Fatura__c f
                          		 where f.ID =: Trigger.New[0].Id Limit 1];
    
         Produto__c prod = [Select ID, Quantidade_em_Estoque__c from Produto__c
                                 where ID =: fat.Produto__r.ID Limit 1];
       
       
            if(fat.Produto__r.ID == prod.Id){
            
               if((Trigger.New[0].Quantidade_de_Produtos__c < 
                   Trigger.Old[0].Quantidade_de_Produtos__c ) || 
                  (prod.Quantidade_em_Estoque__c > 
                   Trigger.New[0].Quantidade_de_Produtos__c)){
                           
                           if(Trigger.New[0].Quantidade_de_Produtos__c > 
                       			Trigger.Old[0].Quantidade_de_Produtos__c){
                                    
                               	prod.Quantidade_em_Estoque__c -= 
                         		(Trigger.New[0].Quantidade_de_Produtos__c - 
                                 Trigger.Old[0].Quantidade_de_Produtos__c);

                           }else{
                                    
                              prod.Quantidade_em_Estoque__c += 
                         	    (Trigger.Old[0].Quantidade_de_Produtos__c - 
                                 Trigger.New[0].Quantidade_de_Produtos__c);
                
                          }
                              
                    }else {
                        
                        if(prod.Quantidade_em_Estoque__c < 
                           (Trigger.New[0].Quantidade_de_Produtos__c - 
                            Trigger.Old[0].Quantidade_de_Produtos__c)){
                                
                           Trigger.New[0].AddError('Não há produtos suficientes no estoque!');
                        }else{
                            prod.Quantidade_em_Estoque__c -= 
                                (Trigger.New[0].Quantidade_de_Produtos__c - 
                                 Trigger.Old[0].Quantidade_de_Produtos__c);
                        }         
                         
                    }
                update prod;
             }
                    
                    
    }catch(Exception e){  Trigger.New[0].AddError(e.getMessage());  }
    
}